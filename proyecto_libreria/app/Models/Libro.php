<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Libro extends Model
{
    use HasFactory;
    //my code
    protected $fillable=[
        'titulo',
        'editorial_id',
        'edicion',
        'pais',
        'precio',
        'editorial_id',
    ];
    public function tipo_documento()
    {
        return $this->belongsTo(Editorial::class,'editorial_id' , 'id');
    }
}
