<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use Illuminate\Http\Request;
//my code for delete
use Illuminate\Support\Facades\Storage;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $data['libros']=Libro::paginate(50);
        return view('books.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //MY CODE
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
                //MY CODE

        //validar campos que se llenen obligatoriamente, con ciertas restricciones
        $campos=[
            'titulo'=>'required|string|max:100',
            'editorial_id' => 'required|exists:editorials,id',
            'edicion' => 'required|numeric|max:99',
            'pais' => 'required|string|max:50',
            'precio' => 'required|numeric|max:1000.99',
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido'
        ];
        //uni ambos
        // que todos los datos que me estan enviando(request) que valide los campos y que muestre los mensajes
        $this->validate($request, $campos,$mensaje);

        // get data from form excepted _token
        $Bookdata= request()->except('_token');

        //put name to image field

        Libro::insert($Bookdata);
        // insert to xammp table
        

        //show in json file
        //return response()->json($Bookdata);
        return redirect('books')->with('mensaje','Libro agregado con exito');
    }

    /**
     * Display the specified resource.
     */
    public function show(Libro $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //guardamos ese book en $libro
        $libr=Libro::findOrFail($id);
        //lo enviamos los datos a books/edit
        return view('books.edit', compact('libr'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //validar campos que se llenen obligatoriamente, con ciertas restricciones
        $campos=[
            'titulo'=>'required|string|max:100',
            'editorial_id'=>'required|max:1',
            'edicion'=>'required|max:2',
            'pais'=>'required|string|max:50',
            'precio'=>'required|float|max:1000,99',
            // 'mail'=>'required|string|max:20;'
        ];
        $mensaje=[
            'required'=>'El :attribute es requerido'
            // 'image.required'=>'Inserte una imagen'
        ];
        //para validar la imagen esta, el usuario no necesariamente tiene que adjuntar una foto
        // que todos los datos que me estan enviando(request) que valide los campos y que muestre los mensajes
        $this->validate($request, $campos,$mensaje);

        //recepcionar la informacion a excepcion del token y el method desde 
        $Bookdata= request()->except(['_token','_method']);
        //va actualizar los datos donde el id es igual a $id
        Book::where('id','=',$id)->update($Bookdata);

        //si existe una foto subida, elimina el antiguo y carga el nuevo
       
        //recuperar la informacion del empleado
        $libro=Book::findOrFail($id);
        //borre la imagen

        //guardamos ese book en $libro con ese id
        $libr=Book::findOrFail($id);
        //lo enviamos los datos a books/edit o mostrar
        //return view('books.edit', compact('libr'));

        return redirect('books')->with('mensaje','Libro modificado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //encontrar el libro
        $libro=Book::findOrFail($id);
        //borramos en bd el libro
        Book::destroy($id);
        // una vez eliminado redireccionamos la url de regreso
        return redirect('books')->with('mensaje','Libro borrado');
    }
}
