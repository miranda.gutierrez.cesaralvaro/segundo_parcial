<?php

namespace Database\Seeders;
use App\Models\Editorial;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EditorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //mi codigo
        // Editorial::create(
        //     [
        //         'id' => 1,
        //         'nombre' => 'juan perez'
        //     ]);
        //     Editorial::create([
        //         'id' => 2,
        //         'nombre' => 'cesar alvaro miranda g'
        //     ]);
        //     Editorial::create([
        //         'id' => 3,
        //         'nombre' => 'jaime lozano'
        //     ]);
        //     Editorial::create([
        //         'id' => 4,
        //         'nombre' => 'don bosco'
        //     ]);
        //     Editorial::create([
        //         'id' => 5,
        //         'nombre' => 'manuel de la quintana'
        //     ]);
        //     Editorial::create([
        //         'id' => 6,
        //         'nombre' => 'don juan'
        //     ]);
        //     Editorial::create([
        //         'id' => 7,
        //         'nombre' => 'el inge'
        //     ]);
        //     Editorial::create([
        //         'id' => 8,
        //         'nombre' => 'el amigo perez'
        //     ]);
        //     Editorial::create([
        //         'id' => 9,
        //         'nombre' => 'los demas del curso'
        //     ]);
        //     Editorial::create([
        //         'id' => 10,
        //         'nombre' => 'ramon castillo'
        //     ]);
        Editorial::factory(10)->create();
    }
}
