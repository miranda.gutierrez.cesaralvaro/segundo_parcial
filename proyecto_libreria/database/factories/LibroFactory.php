<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            //my code
            'titulo' =>fake()->firstname(),
            'editorial_id'=> fake()->numberBetween(1, 5),
            'edicion'=> fake()->numberBetween(0, 1),
            'pais'=>fake()->country(),
            'precio' =>fake()->randomFloat(2, 0, 9999.99),
        ];
    }
}
