@extends('layouts.app')
@section('content')
<div class="container">
<!-- $libr viene del controller desde la funcion update -->
<form action="{{url('/books/'.$libr->id)}}" method="post" enctype="multipart/form-data">
    <!-- token -->
@csrf
<!-- method -->
{{method_field('PATCH')}}
@include('books.form',['modo'=>'Editar']);

</form>
</div>
@endsection
