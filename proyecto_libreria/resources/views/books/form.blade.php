<h1>{{ $modo }} Libro</h1>

@if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
<label for="titulo">Titulo:</label>
<input class="form-control" type="text" name="titulo" value="{{isset($libr->titulo)?$libr->titulo:old('titulo')}}" id="titulo"><br>
</div>

<div class="form-group">
<label for="editorial">Editorial:</label>
<input class="form-control" type="text" name="editorial" value="{{isset($libr->editorial)?$libr->editorial:old('editorial')}}" id="editorial"><br>
</div>

<div class="form-group">
<label for="edicion">Edicion:</label>
<input class="form-control" type="text" name="edicion" value="{{isset($libr->edicion)?$libr->edicion:old('edicion')}}" id="edicion"><br>
</div>

<div class="form-group">
<label for="pais">Pais:</label>
<input class="form-control" type="text" name="pais" value="{{isset($libr->pais)?$libr->pais:old('pais')}}" id="pais"><br>
</div>

<div class="form-group">
    <label for="created_at">Fecha de Creación:</label>
    <input class="form-control" type="date" name="created_at" value="{{ isset($libr->created_at) ? $libr->created_at : old('created_at') }}" id="created_at"><br>
</div>

<div class="form-group">
<label for="updated_at">Fecha de Actualización:</label>
<input class="form-control" type="date" name="updated_at" value="{{isset($libr->updated_at)?$libr->updated_at:old('updated_at')}}" id="updated_at"><br>
</div>

<input class="btn btn-success" type="submit" value="{{ $modo }} datos">
<a class="btn btn-primary" href="{{url('books/')}}">Regresar</a>