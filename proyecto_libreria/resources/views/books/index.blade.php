@extends('layouts.app')
@section('content')
<div class="container">


<!-- si recibimos un mensaje de un formulario -->
@if(Session::has('mensaje'))
<!-- el mensaje con estilo -->
<div class="alert alert-success alert-dismissible" role="alert">
<!-- vamos mostrar dicho mensaje -->
{{Session::get('mensaje')}}
<!-- el boton para desaparecer el mensaje alert -->
<button type="button" class="close btn" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
@endif



<a href="{{url('books/create')}}" class="btn btn-success">Registrar nuevo libro</a>
<table class="table table-light">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>titulo</th>
            <th>editorial_id</th>
            <th>edicion</th>
            <th>pais</th>
            <th>precio</th>
        </tr>
    </thead>
    <tbody>
        @foreach($libros as $book)
        <tr>
            <td>{{$book->id}}</td>
            <td>{{$book->titulo}}</td>
            <td>{{$book->editorial_id}}</td>
            <td>{{$book->edicion}}</td>
            <td>{{$book->pais}}</td>
            <td>{{$book->precio}}</td>
            <td>
                <!-- le pasamos la url concatenado el id y el archivo para editar -->
                <!-- se usa la funcion edit de bookController.php -->
               <a href="{{url('/books/'.$book->id.'/edit')}}" class="btn btn-warning">
                    Editar
               </a>
                <form action="{{url('/books/'.$book->id)}}" class="d-inline" method="post">
                    @csrf
                    {{method_field('DELETE')}}
                    <input class="btn btn-danger" type="submit" onclick="return confirm('Quieres Borrar?')"
                    value="Borrar">
                </form>    
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>#</th>
        </tr>
    </tfoot>
</table>
{!! $libros->links() !!}

</div>
@endsection
