<!-- incluir el menu -->
@extends('layouts.app')
@section('content')
<div class="container">

<!-- estos datos se mandaran al url de este action, el cual web.php lo enrutaraa esa url a un Controller probablemente, 
y si a un controller le llega datos, lo recibe la funcion store y lo mas seguro se carga a la base de datos-->
<form action="{{ url('/books') }}" method="post" enctype="multipart/form-data">
    @csrf <!-- Agrega esto para proteger el formulario contra ataques CSRF -->
    @include('books.form',['modo'=>'Crear'] );

</form>
</div>
@endsection

