# Segundo Parcial

## MIRANDA GUTIERREZ CESAR ALVARO

[PASOS PARA  EJECUTAR ESTE PROYECTO DE LARAVEL]

instalar composer

instalar node.js

crear en mysql phpmydamin la base de datos llamado: com350_seg_par_libreria

MIGRACIONES:
php artisan migrate --seed

INSTALAR TAILWIND:
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p

EN tailwind.config.js reemplasar el content:

  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],

in resources/css/app.css
@tailwind base;
@tailwind components;
@tailwind utilities;

INSTALAR LO NECESARIO 'UI':
composer require laravel/ui

INSTALAR BOOSTRAP:
php artisan ui bootstrap --auth

npm run dev

EJECUTAR:
php artisan serve
